import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { PersonajesComponent } from './personajes/personajes.component';
import { EstudiantesComponent } from './estudiantes/estudiantes.component';
import { ProfesoresComponent } from './profesores/profesores.component';
import { ComponentsModule } from '../components/components.module';
import { SlytherinComponent } from './slytherin/slytherin.component';
import { GryffindorComponent } from './gryffindor/gryffindor.component';
import { RavenclawComponent } from './ravenclaw/ravenclaw.component';
import { HufflepuffComponent } from './hufflepuff/hufflepuff.component';
import { TablehogwartsComponent } from './tablehogwarts/tablehogwarts.component';

//externos
import {NgxPaginationModule} from 'ngx-pagination'; 
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  declarations: [
    HomeComponent,
    PersonajesComponent,
    EstudiantesComponent,
    ProfesoresComponent,
    SlytherinComponent,
    GryffindorComponent,
    RavenclawComponent,
    HufflepuffComponent,
    TablehogwartsComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    NgxPaginationModule,
    PipesModule,
    
  ]
})
export class PagesModule { }
