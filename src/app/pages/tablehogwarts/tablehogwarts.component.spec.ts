import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablehogwartsComponent } from './tablehogwarts.component';

describe('TablehogwartsComponent', () => {
  let component: TablehogwartsComponent;
  let fixture: ComponentFixture<TablehogwartsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablehogwartsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TablehogwartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
