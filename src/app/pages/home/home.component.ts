import { Component, OnInit } from '@angular/core';
import { AttributesResponse } from 'src/app/interfaces/Attributes-response';
import { EncantamientosService } from 'src/app/services/encantamientos.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public casaHogwarts: AttributesResponse[]=[]

  constructor(private encantamientoService: EncantamientosService) {
     
    
  
   }

  ngOnInit(): void {
  }

}
