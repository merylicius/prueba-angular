import { Component, OnInit } from '@angular/core';
import { AttributesResponse } from 'src/app/interfaces/Attributes-response';
import { EncantamientosService } from 'src/app/services/encantamientos.service';

@Component({
  selector: 'app-gryffindor',
  templateUrl: './gryffindor.component.html',
  styleUrls: ['./gryffindor.component.css']
})
export class GryffindorComponent implements OnInit {

  public gryffindor:AttributesResponse[]=[]

  constructor(private encantamientoService: EncantamientosService) { 

    this.encantamientoService.getCasaHogwarts('gryffindor')
    .subscribe(resp=>this.gryffindor=resp)
  }

  ngOnInit(): void {

  }

}
