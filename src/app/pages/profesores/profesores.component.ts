import { Component, OnInit } from '@angular/core';
import { AttributesResponse } from 'src/app/interfaces/Attributes-response';
import { EncantamientosService } from 'src/app/services/encantamientos.service';

@Component({
  selector: 'app-profesores',
  templateUrl: './profesores.component.html',
  styleUrls: ['./profesores.component.css']
})
export class ProfesoresComponent implements OnInit {

  public profesores: AttributesResponse[]=[]

  constructor(private encantamientoService: EncantamientosService) {

    this.encantamientoService.getProfesoresHogwarts()
    .subscribe(resp=>this.profesores=resp)
   }

  ngOnInit(): void {
  }

}
