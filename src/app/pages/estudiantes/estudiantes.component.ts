import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AttributesResponse } from 'src/app/interfaces/Attributes-response';
import { EncantamientosService } from 'src/app/services/encantamientos.service';

@Component({
  selector: 'app-estudiantes',
  templateUrl: './estudiantes.component.html',
  styleUrls: ['./estudiantes.component.css']
})
export class EstudiantesComponent implements OnInit {

  public estudiantes:AttributesResponse[]=[]

  forma?: FormGroup;


  constructor(private encantamientoService: EncantamientosService , private fb: FormBuilder) { 

    this.encantamientoService.getEstudiantesHogwarts()
    .subscribe(resp=>this.estudiantes=resp); 
    this.crearFormulario();

  }

  ngOnInit(): void {
  }

  crearFormulario(){
this.forma = this.fb.group({
  nombre:['', [Validators.required, Validators.minLength(3)]],
  edad: ['', [Validators.required, Validators.minLength(4)]],
  casa: [''],
  patronus: ['',[Validators.required, Validators.minLength(4)]]
})
  }

  guardar(){
    console.log(this.forma)
  }

}
