import { Component, OnInit } from '@angular/core';
import { AttributesResponse } from 'src/app/interfaces/Attributes-response';
import { EncantamientosService } from 'src/app/services/encantamientos.service';

@Component({
  selector: 'app-slytherin',
  templateUrl: './slytherin.component.html',
  styleUrls: ['./slytherin.component.css']
})
export class SlytherinComponent implements OnInit {

  public slytherin: AttributesResponse[]=[]

  constructor(private encantamientoService: EncantamientosService) { 

    this.encantamientoService.getCasaHogwarts('slytherin')
    .subscribe(resp=>this.slytherin=resp)
  }

  ngOnInit(): void {

 
  }


  

}
