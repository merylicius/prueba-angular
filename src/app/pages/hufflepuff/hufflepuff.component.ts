import { Component, OnInit } from '@angular/core';
import { AttributesResponse } from 'src/app/interfaces/Attributes-response';
import { EncantamientosService } from 'src/app/services/encantamientos.service';

@Component({
  selector: 'app-hufflepuff',
  templateUrl: './hufflepuff.component.html',
  styleUrls: ['./hufflepuff.component.css']
})
export class HufflepuffComponent implements OnInit {

  public hufflepuff: AttributesResponse[]=[]

  constructor(private encantamientoService: EncantamientosService) { 

    this.encantamientoService.getCasaHogwarts('hufflepuff')
    .subscribe(resp=>this.hufflepuff = resp)
  }

  ngOnInit(): void {
  }

}
