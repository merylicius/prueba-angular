import { Component, OnInit } from '@angular/core';
import { AttributesResponse } from 'src/app/interfaces/Attributes-response';
import { EncantamientosService } from 'src/app/services/encantamientos.service';

@Component({
  selector: 'app-ravenclaw',
  templateUrl: './ravenclaw.component.html',
  styleUrls: ['./ravenclaw.component.css']
})
export class RavenclawComponent implements OnInit {
  public revenclaw: AttributesResponse[]=[]

  constructor(private encantamientoService: EncantamientosService) {

        this.encantamientoService.getCasaHogwarts('ravenclaw')
    .subscribe(resp=>this.revenclaw=resp)

   }

  ngOnInit(): void {
  }

}
