import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'age'
})
export class AgePipe implements PipeTransform {

  transform(age: any): any{
    console.log(age)
    age = new Date(age);
    var otherDate = new Date();

    var years = (otherDate.getFullYear() - age);

    if (otherDate.getMonth() < age.getMonth() || 
        otherDate.getMonth() === age.getMonth() && otherDate.getDate() < age.getDate()) {
        years--;
    }
    return years;
   
  
  }
  
}
