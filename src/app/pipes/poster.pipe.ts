import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'poster'
})
export class PosterPipe implements PipeTransform {

  transform(poster: string): string {

    
    if(poster){
      return `${poster}`
    }else{
      return './assets/no-image.jpg'
    }
    
  }

}
