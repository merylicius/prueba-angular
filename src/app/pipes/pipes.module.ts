import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PosterPipe } from './poster.pipe';
import { AgePipe } from './age.pipe';



@NgModule({
  declarations: [
    PosterPipe,
    AgePipe
  ],
  exports:[
    PosterPipe,
    AgePipe
  ],
  imports: [
    CommonModule
  ]
})
export class PipesModule { 

  

}
