import { TestBed } from '@angular/core/testing';

import { EncantamientosService } from './encantamientos.service';

describe('EncantamientosService', () => {
  let service: EncantamientosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EncantamientosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
