import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AttributesResponse } from '../interfaces/Attributes-response';


@Injectable({
  providedIn: 'root'
})
export class EncantamientosService {

  constructor(private http: HttpClient) { }

  getCasaHogwarts(casa:string):Observable<AttributesResponse[]>{
    return this.http.get<AttributesResponse[]>(`http://hp-api.herokuapp.com/api/characters/house/${casa}`)
  }

  getEstudiantesHogwarts():Observable<AttributesResponse[]>{
    return this.http.get<AttributesResponse[]>('http://hp-api.herokuapp.com/api/characters/students')
  }

  getProfesoresHogwarts():Observable<AttributesResponse[]>{
    return this.http.get<AttributesResponse[]>('http://hp-api.herokuapp.com/api/characters/staff')
  }

}
