import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { EstudiantesComponent } from './pages/estudiantes/estudiantes.component';
import { PersonajesComponent } from './pages/personajes/personajes.component';
import { SlytherinComponent } from './pages/slytherin/slytherin.component';
import { GryffindorComponent } from './pages/gryffindor/gryffindor.component';
import { RavenclawComponent } from './pages/ravenclaw/ravenclaw.component';
import { HufflepuffComponent } from './pages/hufflepuff/hufflepuff.component';
import { ProfesoresComponent } from './pages/profesores/profesores.component';


  const routes: Routes=[
    {
      path:'home',
      component: HomeComponent
    },
    {
      path:'slytherin',
      component: SlytherinComponent
    },
    {
      path:'gryffindor',
      component: GryffindorComponent
    },
    {
      path:'ravenclaw',
      component: RavenclawComponent
    },
    {
      path:'hufflepuff',
      component: HufflepuffComponent
    },
    {
      path:'estudiantes',
      component: EstudiantesComponent
    },
    {
      path:'profesores',
      component: ProfesoresComponent
    },
    {
      path:'**',
      redirectTo:'/home'
    },
    
  ]



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports:[
    RouterModule
  ]
})
export class AppRoutingModule { }
